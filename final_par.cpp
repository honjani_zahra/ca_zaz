#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <stdio.h>  
#include <sstream>
#include <time.h> 
#include <bits/stdc++.h> 
using namespace std;
#define NUMBER_OF_THREADS 4
pthread_t threads[NUMBER_OF_THREADS];
vector <double> min_all;
vector <double> max_all;
bool firstone;
double sum_accuracy;
double length;
int attr_length;
vector< vector<double> > weights_values;
vector< vector<double> > correct;

struct read_csv_arg
{
	vector< vector<double> > csv_values;
	string file_name;
	long index;

};
struct read_csv_arg structs[NUMBER_OF_THREADS];

pthread_mutex_t mutex_minmax;
pthread_mutex_t mutex_sum;

void read_csv_wieght(vector< vector<double> > &csv_values, string file_name)
{
    fstream file(file_name);

    if (file)
    {

        string line;

        while (getline(file, line))
        {
            std::stringstream ss(line);
            vector<double> values;
            while(ss.good())
            {
                string substr;
                getline( ss, substr, ',' );
                values.push_back(atof(substr.c_str()));
            }
            csv_values.push_back(values);
        }
    }
	cout.precision(2);
    cout.setf(ios::fixed,ios::floatfield);

}

void normalize_column(int index, int j)
{
	double max = max_all[j];
	double min = min_all[j];
	double r = max - min;
	for(int i = 0; i < structs[index].csv_values.size(); i++)
	{
		structs[index].csv_values[i][j] = (structs[index].csv_values[i][j] - min)/r;
	}
}

void* normalize(void* arg)
{
	long index = (long)arg;
	for(int j = 0; j < attr_length ; j++)
	{
		normalize_column(index, j);
	}

}

double inner_product(vector<double> &A, vector<double> &B)
{
	double answer;
	for(int i = 0; i < A.size() - 1; i++)
	{
		answer += A[i]*B[i];
	}
	answer += B[B.size() - 1];
	return answer;
}

double score(int index , int num1, int num2)
{
	return inner_product(structs[index].csv_values[num1], weights_values[num2]);
}

int predict_class(int index , int number)
{
	vector<double> scors;
	scors.push_back(score(index,  number, 1));
	scors.push_back(score(index,  number, 2));
	scors.push_back(score(index,  number, 3));
	scors.push_back(score(index,  number, 4));
	double predict = scors[0];
	int index_most = 0;
	for(int i = 0; i < 4; i++)
	{
		if(scors[i] > predict)
		{
			predict = scors[i];
			index_most = i;
		}
	}
	return index_most;
}

double accuracy(long index, vector <double> pred)
{
	int count = 0;
	for(int i = 0; i < length; i++)
	{
		if(pred[i] == correct[index][i])
		{
			count += 1;
		}
	}
	double accuracy_value = ((float)count)*100;
	return accuracy_value;
}

void* predict_thread (void* arg)
{
	long index = (long) arg;
	vector <double> pred;
	for(int k = 0; k < structs[index].csv_values.size() ; k++)
	{
		pred.push_back(predict_class(index ,  k));
	}
	double accuracy_thread = accuracy(index , pred) ; 
	pthread_mutex_lock (&mutex_sum);
	sum_accuracy += accuracy_thread;
	pthread_mutex_unlock (&mutex_sum);
}

void count_line(string file_name)
{
	fstream file(file_name);
	int line_counter = 0 , attr_counter = 0;
	string line , attr;
	getline(file , line);

	std::stringstream ss(line);
	while (getline(ss , attr , ','))
		attr_counter++;
	attr_length = attr_counter;
	
	while (getline(file , line))
		line_counter++;
	length = line_counter / NUMBER_OF_THREADS;
}

void make_files(string file_name)
{
	
	fstream file(file_name);
	int line_counter = 0;
    string line, startline , file_id;

	getline(file, line);
	startline = line;
    if (file)
    {
    	
		for (int i=0 ; i<NUMBER_OF_THREADS ; i++)
        {
			line_counter = 0;
			file_id = "datasets/train_" + to_string(i) + ".csv";
			fstream train(file_id , fstream::out);

			train << startline << endl; 
			while (line_counter < length )
			{
				getline(file, line);
				line_counter++;
				train << line <<endl;
			}
		}
    }
}

void* read_csv(void* arg)
{
	struct read_csv_arg *arg1 = (struct read_csv_arg*)arg;
	string file_name;
	double my_sum = 0;
	double min_in_thread [attr_length];
	double max_in_thread [attr_length];
	file_name = arg1->file_name;
    fstream file(file_name);
	vector< vector<double> > result;

    if (file)
    {
    	int line_counter = 0;
        string line , substr;
		getline(file, line);
		while (getline(file, line))
        {
            std::stringstream ss(line);
            vector<double> values;
			int column_counter = 0;
            while(ss.good())
            {
                string substr;
                getline( ss, substr, ',' );
                values.push_back(atof(substr.c_str()));

				// max-min 
				if(line_counter == 0)	
				{
					min_in_thread[column_counter] = atof(substr.c_str());
					max_in_thread[column_counter] = min_in_thread[column_counter];
				}
				else if(min_in_thread[column_counter] > atof(substr.c_str()))
					min_in_thread[column_counter] = atof(substr.c_str());

				else if(max_in_thread[column_counter] < atof(substr.c_str()))
					max_in_thread[column_counter] = atof(substr.c_str());
				//	
				column_counter ++;
            }

            result.push_back(values);
			line_counter++;
        }
    }
    arg1->csv_values = result;

	pthread_mutex_lock (&mutex_minmax);
	if (firstone)
	{
		for(int i=0 ; i<attr_length ; i++)
		{
			min_all[i] = min_in_thread[i];
			max_all[i] = max_in_thread[i];
		}
		firstone = false;
	}
	for(int i=0 ; i<attr_length ; i++)
	{
		if(min_all[i] > min_in_thread[i])	min_all[i] = min_in_thread[i];
		if(max_all[i] < max_in_thread[i])	max_all[i] = max_in_thread[i];
	}
	pthread_mutex_unlock (&mutex_minmax);
	pthread_exit((void*)0);
}

void set_globals()
{
	correct.resize(NUMBER_OF_THREADS , vector <double>(length));
	min_all.resize(attr_length);
	max_all.resize(attr_length);
	firstone = true;
}


int main(int argc, char* argv[])
{
	string name = argv[1];
	string file_name_t = name + "train.csv";
	string file_name_w = name + "weights.csv";
	double accuracy_thread [NUMBER_OF_THREADS];
	
	vector< vector<double> > train_values;
	read_csv_wieght(weights_values,file_name_w);
	pthread_mutex_init(&mutex_minmax, NULL);
	pthread_mutex_init(&mutex_sum, NULL);
	count_line(file_name_t);
	set_globals();
	make_files(file_name_t);

	for(long i = 0; i < NUMBER_OF_THREADS; i++)
	{
		structs[i].file_name = "datasets/train_" + to_string(i) + ".csv";
		structs[i].index = i;
		pthread_create(&threads[i], NULL, read_csv, (void*)&structs[i]);
	}

	for(long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_join(threads[i], NULL);
	for (int l = 0 ; l < NUMBER_OF_THREADS ; l++)
	{
		for(int k = 0; k < structs[l].csv_values.size() ; k++)
		{
			correct[l][k] = structs[l].csv_values[k][20];
		}
	}
	for(long i = 0; i< NUMBER_OF_THREADS; i++)
	{
		pthread_create(&threads[i], NULL, normalize, (void*)i);
	}
	for(long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_join(threads[i], NULL);
	sum_accuracy = 0 ;
	for(long i = 0; i< NUMBER_OF_THREADS; i++)
	{
		pthread_create(&threads[i], NULL, predict_thread, (void*)i);
	}
	for(long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_join(threads[i], NULL);

	cout<< "‫‪Accuracy‬‬: " << sum_accuracy / (length*NUMBER_OF_THREADS) << "%"<< endl;
	pthread_exit(NULL);
}