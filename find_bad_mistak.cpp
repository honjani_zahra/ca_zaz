#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <stdio.h> 
#include <sstream>
#include <time.h> 
using namespace std;
#define length 500
#define NUMBER_OF_THREADS 4
pthread_t threads[NUMBER_OF_THREADS];
double min_all[21];
double max_all[21];
bool firstone;

struct read_csv_arg
{
	vector< vector<double> > *csv_values;
	string file_name;
	long index;

};
struct read_csv_arg structs[NUMBER_OF_THREADS];

pthread_mutex_t mutex_minmax;
pthread_mutex_t mutex_sum;

void read_csv_wieght(vector< vector<double> > &w_values, string file_name)
{
    fstream file(file_name);

    if (file)
    {

        string line;

        while (getline(file, line))
        {
            std::stringstream ss(line);
            vector<double> values;
            while(ss.good())
            {
                string substr;
                getline( ss, substr, ',' );
                values.push_back(atof(substr.c_str()));
            }
            // store array of values
            w_values.push_back(values);
        }
    }
    // display results
	cout.precision(2);
    cout.setf(ios::fixed,ios::floatfield);

}

void normalize_column(int index, int j)
{
	double max = max_all[j];
	double min = min_all[j];
	double r = max - min;
	for(int i = 0; i < 500; i++)
	{
		//cout<< "gheire normali  =  " << structs[index].csv_values->at(i).at(j)<< endl;
		structs[index].csv_values->at(i).at(j) = ((structs[index].csv_values->at(i).at(j)) - min)/r;
		//cout<< "normali  =  "<< structs[index].csv_values->at(i).at(j)<< endl;
	}
}

void* normalize(void* arg)
{
	long index = (long)arg;
	for(int j = 0; j < 21; j++)
	{
		// cout<< " raft1"<< endl;
		normalize_column(index, j);
		// cout<< "raft2"<< endl;
	}

}

double inner_product(vector<double> &A, vector<double> &B)
{
	double answer;
	for(int i = 0; i < A.size() - 1; i++)
	{
		answer += A[i]*B[i];
	}
	answer += B[B.size() - 1];
	return answer;
}

double score(vector< vector<double> > &train_values, vector< vector<double> > &weights_values, int num1, int num2)
{
	return inner_product(train_values[num1], weights_values[num2]);
}

int predict_class(vector< vector<double> > &train_values, vector< vector<double> > &weights_values, int number)
{
	vector<double> scors;
	scors.push_back(score(train_values, weights_values, number, 1));
	scors.push_back(score(train_values, weights_values, number, 2));
	scors.push_back(score(train_values, weights_values, number, 3));
	scors.push_back(score(train_values, weights_values, number, 4));
	double predict = scors[0];
	int index = 0;
	for(int i = 0; i < 4; i++)
	{
		if(scors[i] > predict)
		{
			predict = scors[i];
			index = i;
		}
	}
	return index;
}
double accuracy(vector<double> &correct, vector<double> &pred)
{
	int count = 0;
	for(int i = 0; i < pred.size(); i++)
	{
		if(pred[i] == correct[i])
		{
			count += 1;
		}
	}
	double accuracy_value = ((float)count)*100/(float)(pred.size());
	return accuracy_value;
}

void make_files(string file_name, int index)
{
	
	fstream file(file_name);

	int line_counter = 0;
    string line, startline , file_id;

	getline(file, line);
	startline = line;
    if (file)
    {
    	
		for (int i=0 ; i<4 ; i++)
        {
			line_counter = 0;
			file_id = "train_" + to_string(i) + ".csv";
			fstream train(file_id , fstream::out);

			train << startline << endl; 
			while (line_counter < length )
			{
				getline(file, line);
				line_counter++;
				train << line <<endl;
			}
		}
    }
}

void* read_csv(void* arg)
{
	//struct read_csv_arg *arg1 = (struct read_csv_arg*)arg;
	long index_thread = (long) arg;
	string file_name;
	double my_sum = 0;
	double min_in_thread [21];
	double max_in_thread [21];
	// cout<< "index"<< arg1->file_name << endl;

	file_name = structs[index_thread].file_name;
    fstream file(file_name);
	vector< vector<double> > result;
	vector<double> value0;
	for(int k= 0 ; k<21 ; k++)
	{
		value0.push_back(0);
	}

	for(int i = 0 ; i<8 ; i++)
	{
		result.push_back(value0);
	}
    if (file)
    {
    	int line_counter = 0;
        string line , substr;
		getline(file, line);
		while (getline(file, line))
        {
            std::stringstream ss(line);
            vector<double> values;
			int column_counter = 0;
            while(column_counter < 21)
            {
                string substr;
                getline( ss, substr, ',' );
                values.push_back(atof(substr.c_str()));

				// max-min 
				if(line_counter == 0)	
				{
					// if (column_counter == 0)	cout<<substr <<"  "<< values[0] <<endl;
					min_in_thread[column_counter] = atof(substr.c_str());
					max_in_thread[column_counter] = min_in_thread[column_counter];
				}
				else if(min_in_thread[column_counter] > atof(substr.c_str()))
					min_in_thread[column_counter] = atof(substr.c_str());

				else if(max_in_thread[column_counter] < atof(substr.c_str()))
					max_in_thread[column_counter] = atof(substr.c_str());
				//	
				column_counter ++;
            }

            result.push_back(values);
			line_counter++;
        }
    }
    cout<< "res "<< result[8][0]<<endl;
	pthread_mutex_lock (&mutex_sum);
    structs[index_thread].csv_values = &result;
	pthread_mutex_unlock (&mutex_sum);

	pthread_mutex_lock (&mutex_minmax);
	// if (firstone)
	// {
	// 	for(int i=0 ; i<21 ; i++)
	// 	{
	// 		min_all[i] = min_in_thread[i];
	// 		max_all[i] = max_in_thread[i];
	// 	}
	// 	firstone = false;
	// }
	// for(int i=0 ; i<21 ; i++)
	// {
	// 	if(min_all[i] > min_in_thread[i])	min_all[i] = min_in_thread[i];
	// 	if(max_all[i] < max_in_thread[i])	max_all[i] = max_in_thread[i];
	// }
	

	pthread_mutex_unlock (&mutex_minmax);
	// if(structs[0].csv_values->at(0).at(0))	cout<<"sefre" <<structs[0].csv_values->at(0).at(0) <<endl;
	pthread_exit((void*)0);
}

int main()
{
	string file_name_t = "train.csv";
	string file_name_w = "weights.csv";
	vector<double> correct;
	vector<double> pred;
	//vector< vector<double> > train_values;
	vector< vector<double> > weights_values;
	read_csv_wieght(weights_values,file_name_w);
	pthread_mutex_init(&mutex_minmax, NULL);
	pthread_mutex_init(&mutex_sum, NULL);
	// read_csv(train_values, file_name_t);
	// read_csv(weights_values, file_name_w);
	// for(int k = 1; k < train_values.size(); k++)
	// {
	// 	correct.push_back(train_values[k][20]);
	// }
	// normalize(train_values);
	// for(int i = 1; i < train_values.size(); i++)
	// {
	// 	pred.push_back(predict_class(train_values, weights_values, i));
	// }
	// double result = accuracy(correct,pred);
	// cout<< "the accuracy of model is  "<< result<< endl;

	make_files("train.csv", 0);
	firstone = true;

	for(long i = 0; i < NUMBER_OF_THREADS; i++)
	{
		structs[i].file_name = "train_" + to_string(i) + ".csv";
		//structs[i].csv_values = &train_values;
		structs[i].index = i;
		pthread_create(&threads[i], NULL, read_csv, (void*)i);
	}
	for(long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_join(threads[i], NULL);
	
	for(int i = 0; i < 4 ; i++)
		cout<<structs[i].csv_values->at(8).at(0)<<"  "<<endl;;

	// for(long i = 0; i< NUMBER_OF_THREADS; i++)
	// {
	// 	pthread_create(&threads[i], NULL, normalize, (void*)i);
	// }
	// for(long i = 0; i < NUMBER_OF_THREADS; i++)
	// 	pthread_join(threads[i], NULL);

	// fstream train_file("normal_seri_par" , fstream::out);
	// for(int i = 0 ; i < NUMBER_OF_THREADS ; i++)
	// {
	// 	for(int j = 0 ; j < 500 ; j++)
	// 	{
	// 		for(int k = 0 ; k < 21 ; k++)
	// 		{
	// 			train_file << structs[i].csv_values->at(j).at(k) << "   "; 
	// 		}
	// 		train_file <<endl;
	// 	}
	// }

	pthread_exit(NULL);
}