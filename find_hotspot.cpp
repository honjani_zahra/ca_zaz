#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <stdio.h> 
#include <sstream>
#include <chrono> 
#include <time.h> 
using namespace std::chrono; 
using namespace std;
double sum_ip = 0;
double sum_sc = 0;



void normalize_column(vector< vector<double> > &train_values, int j)
{
	double max = train_values[1][j];
	double min = train_values[1][j];
	for(int i = 1; i < train_values.size(); i++)
	{
		if(train_values[i][j] < min)
		{
			min = train_values[i][j];
		}
		if(train_values[i][j] > max)
		{
			max = train_values[i][j];
		}
	}
	double r = max - min;
	for(int i = 1; i < train_values.size(); i++)
	{
		train_values[i][j] = (train_values[i][j] - min)/r;
	}
}
//behine sazi aynaz
void normalize(vector< vector<double> > &train_values)
{
	auto start_nc = high_resolution_clock::now();
	for(int j = 0; j < 20; j++)
	{
		normalize_column(train_values, j);
	}
	auto stop_nc = high_resolution_clock::now(); 
	auto duration_nc = duration_cast<microseconds>(stop_nc - start_nc);
	cout<< "duration normalize_column is =  "<< duration_nc.count()<< endl;

}

double inner_product(vector<double> &A, vector<double> &B)
{
	double answer;
	for(int i = 0; i < A.size() - 1; i++)
	{
		answer += A[i]*B[i];
	}
	answer += B[B.size() - 1];
	return answer;
}

double score(vector< vector<double> > &train_values, vector< vector<double> > &weights_values, int num1, int num2)
{
	auto start_ip = high_resolution_clock::now();
	double temp =  inner_product(train_values[num1], weights_values[num2]);
	auto stop_ip = high_resolution_clock::now(); 
	auto duration_ip = duration_cast<microseconds>(stop_ip - start_ip);
	sum_ip += duration_ip.count();
	// cout<< "duration of inner_product is =  "<< duration_ip.count()<< endl;
	return temp;
}

int predict_class(vector< vector<double> > &train_values, vector< vector<double> > &weights_values, int number)
{
	vector<double> scors;
	auto start_sc = high_resolution_clock::now();
	scors.push_back(score(train_values, weights_values, number, 1));
	scors.push_back(score(train_values, weights_values, number, 2));
	scors.push_back(score(train_values, weights_values, number, 3));
	scors.push_back(score(train_values, weights_values, number, 4));
	auto stop_sc = high_resolution_clock::now(); 
	auto duration_sc = duration_cast<microseconds>(stop_sc - start_sc);
	sum_sc += duration_sc.count();
	double predict = scors[0];
	int index = 0;
	for(int i = 0; i < 4; i++)
	{
		if(scors[i] > predict)
		{
			predict = scors[i];
			index = i;
		}
	}
	return index;
}
double accuracy(vector<double> &correct, vector<double> &pred)
{
	int count = 0;
	for(int i = 0; i < pred.size(); i++)
	{
		// cout<< "pred "<< i<< "  is "<< pred[i]<< endl;
		// cout<< "correct "<< i<< "  is "<< correct[i]<< endl;
		// cout<< "raft to for"<< endl;
		if(pred[i] == correct[i])
		{
			// cout<< "raft to if  "<< endl;
			count += 1;
		}
	}
	double accuracy_value = ((float)count)*100/(float)(pred.size());
	return accuracy_value;
}

void read_csv(vector< vector<double> > &csv_values, string file_name)
{
    fstream file(file_name);

    if (file)
    {

        string line;

        while (getline(file, line))
        {
            std::stringstream ss(line);
            vector<double> values;
            while(ss.good())
            {
                string substr;
                getline( ss, substr, ',' );
                values.push_back(atof(substr.c_str()));
            }
            // store array of values
            csv_values.push_back(values);
        }
    }
    

    // display results
    cout.precision(2);
    cout.setf(ios::fixed,ios::floatfield);

    // for (vector< vector<double> >::const_iterator it = csv_values.begin(); it != csv_values.end(); ++it)
    // {
    //     const vector<double>& values = *it;

    //     for (vector<double>::const_iterator it2 = values.begin(); it2 != values.end(); ++it2)
    //     {
    //         cout << *it2 << " ";
    //     }
    //     cout << endl;
    // }
}
int main()
{
	string file_name_t = "train.csv";
	string file_name_w = "weights.csv";
	vector<double> correct;
	vector<double> pred;
	vector< vector<double> > train_values;
	vector< vector<double> > weights_values;
	auto start_rc = high_resolution_clock::now();
	read_csv(train_values, file_name_t);
	read_csv(weights_values, file_name_w);
	auto stop_rc = high_resolution_clock::now(); 
	auto duration_rc = duration_cast<microseconds>(stop_rc - start_rc);
	cout<< "duration_rc "<< duration_rc.count()<< endl; 
	for(int k = 1; k < train_values.size(); k++)
	{
		correct.push_back(train_values[k][20]);
	}
	auto start_n = high_resolution_clock::now();
	normalize(train_values);
	auto stop_n = high_resolution_clock::now(); 
	auto duration_n = duration_cast<microseconds>(stop_n - start_n);
	cout<< "duration_n "<< duration_n.count()<< endl;
	auto start_pc = high_resolution_clock::now();
	for(int i = 1; i < train_values.size(); i++)
	{
		pred.push_back(predict_class(train_values, weights_values, i));
		// cout<<"prediction  "<< i<< "  is "<< predict_class(train_values, weights_values, i);
		// cout<< endl;
	}
	auto stop_pc = high_resolution_clock::now(); 
	auto duration_pc = duration_cast<microseconds>(stop_pc - start_pc);
	cout<< "duration_pc  "<< duration_pc.count()<<endl;
	auto start_acc = high_resolution_clock::now();
	double result = accuracy(correct,pred);
	auto stop_acc = high_resolution_clock::now(); 
	auto duration_acc = duration_cast<microseconds>(stop_acc - start_acc);
	cout<< "duration_acc "<< duration_acc.count()<< endl;
	// accuracy(correct, pred);
	// cout<< "size   "<< train_values.size()<<endl;
	// cout<< "the accuracy of model is  "<< result<< endl;
	cout<< " duration_ip "<< sum_ip<< endl;
	cout<< "duration_sc "<< sum_sc<< endl;



}