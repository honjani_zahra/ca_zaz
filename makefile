EXECUTABLE_FILE = ./PhonePricePrediction.out

CC = g++
CFLAGS = -std=c++11

.PHONY: all make_build_dir build_objects 

all: build_objects

OBJECTS = main.cpp

build_objects:
	$(CC) $(OBJECTS) -o $(EXECUTABLE_FILE)  
