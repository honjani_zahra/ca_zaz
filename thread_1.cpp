#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <stdio.h> 
#include <sstream>
#include <time.h> 
#include <pthread.h>
using namespace std;

#define NUMBER_OF_THREADS 5
#define thread_length 400
pthread_t threads[NUMBER_OF_THREADS];


void normalize_column(vector< vector<double> > &train_values, int j)
{
	double max = train_values[1][j];
	double min = train_values[1][j];
	for(int i = 1; i < train_values.size(); i++)
	{
		if(train_values[i][j] < min)
		{
			min = train_values[i][j];
		}
		if(train_values[i][j] > max)
		{
			max = train_values[i][j];
		}
	}
	double r = max - min;
	for(int i = 1; i < train_values.size(); i++)
	{
		train_values[i][j] = (train_values[i][j] - min)/r;
	}
}
//behine sazi aynaz
void normalize(vector< vector<double> > &train_values)
{
	for(int j = 0; j < 20; j++)
	{
		normalize_column(train_values, j);
	}

}

double inner_product(vector<double> &A, vector<double> &B)
{
	double answer;
	for(int i = 0; i < A.size() - 1; i++)
	{
		answer += A[i]*B[i];
	}
	answer += B[B.size() - 1];
	return answer;
}

double score(vector< vector<double> > &train_values, vector< vector<double> > &weights_values, int num1, int num2)
{
	return inner_product(train_values[num1], weights_values[num2]);
}

int predict_class(vector< vector<double> > &train_values, vector< vector<double> > &weights_values, int number)
{
	vector<double> scors;
	scors.push_back(score(train_values, weights_values, number, 1));
	scors.push_back(score(train_values, weights_values, number, 2));
	scors.push_back(score(train_values, weights_values, number, 3));
	scors.push_back(score(train_values, weights_values, number, 4));
	double predict = scors[0];
	int index = 0;
	for(int i = 0; i < 4; i++)
	{
		if(scors[i] > predict)
		{
			predict = scors[i];
			index = i;
		}
	}
	return index;
}
double accuracy(vector<double> &correct, vector<double> &pred)
{
	int count = 0;
	for(int i = 0; i < pred.size(); i++)
	{
		if(pred[i] == correct[i])
		{
			count += 1;
		}
	}
	double accuracy_value = ((float)count)*100/(float)(pred.size());
	return accuracy_value;
}
struct read_csv_arg
{
	vector< vector<double> > *csv_values;
	char* file_name;
	long index;

};

pthread_mutex_t mutex_sum;


void* read_csv(void* arg)
{
	struct read_csv_arg *arg1 = (struct read_csv_arg*)arg;
	char* file_name;
	double my_sum = 0;
	// struct read_csv_arg arg1 = arg;
	int start = (arg1->index) * (thread_length);
	int end = start + thread_length;
	//cout<<arg1->file_name<<endl;
	file_name = arg1->file_name;
    fstream file(file_name);
	vector< vector<double> > result;
    //cout<< "index"<< arg1->index << endl;

    if (file)
    {
    	int line_counter = 0;
        string line;
        while(line_counter < start)
        {
        	line_counter++;
        	getline(file, line);
        }
        while (getline(file, line) and line_counter < end)
        {
			//cout<<"while"<<endl;
        	line_counter++;
            std::stringstream ss(line);
            vector<double> values;
			pthread_mutex_lock (&mutex_sum);
			cout<<"val size"<<values.size()<<endl;
			pthread_mutex_unlock (&mutex_sum);
            while(ss.good())
            {
                string substr;
                getline( ss, substr, ',' );
                values.push_back(atof(substr.c_str()));
            }
            // store array of values
			pthread_mutex_lock (&mutex_sum);
			cout<<"val size"<<values.size()<<endl;
			pthread_mutex_unlock (&mutex_sum);
        	result.push_back(values);
        }
		//cout<<"resultsize"<<result.size()<<endl;
    }
	//cout<<"here"<<endl;
	pthread_mutex_lock (&mutex_sum);
	arg1->csv_values->insert(arg1->csv_values->end(), result.begin(), result.end() );
	// for(int i = 0 ; i<=result.size() ; i++)
	// {
	// 	arg1->csv_values->push_back(result[i]);
	// }
	cout<<arg1->csv_values->size()<<endl;
	pthread_mutex_unlock (&mutex_sum);


    //display results
    cout.precision(2);
    cout.setf(ios::fixed,ios::floatfield);
	//cout<<"func finish"<<endl;
    pthread_exit((void*)0);
}
int main()
{
	void *status;
	string file_name_t = "train.csv";
	string file_name_w = "weights.csv";
	vector<double> correct;
	vector<double> pred;
	vector< vector<double> > train_values;
	vector< vector<double> > weights_values;
	pthread_mutex_init(&mutex_sum, NULL);

	// read_csv(train_values, file_name_t);
	// read_csv(weights_values, file_name_w);
	// for(int k = 1; k < train_values.size(); k++)
	// {
	// 	correct.push_back(train_values[k][20]);
	// }
	// normalize(train_values);
	// for(int i = 1; i < train_values.size(); i++)
	// {
	// 	pred.push_back(predict_class(train_values, weights_values, i));
	// }
	// double result = accuracy(correct,pred);
	// cout<< "the accuracy of model is  "<< result<< endl;
	struct read_csv_arg arg;
	vector <struct read_csv_arg> structs;
	arg.file_name= "train.csv";
	//fstream file(arg->file_name);
	//etline(file, line);
	//cout<< "sakht"<< endl;
	arg.csv_values = &train_values;
	//cout<< "sakht"<< endl;
	for(long i = 0; i < NUMBER_OF_THREADS; i++)
	{
		structs.push_back(arg);
		structs[i].file_name = "train.csv";
		structs[i].csv_values = &train_values;
		structs[i].index = i;
		pthread_create(&threads[i], NULL, read_csv, (void*)&structs[i]);
		//cout<< "ahhhh "<< endl;
	}

	cout<<"threading done"<<endl;
	for(long i = 0; i < NUMBER_OF_THREADS; i++)
	{
		cout<<"joining"<<endl;
		pthread_join(threads[i], &status);
		cout<< "ahhhhkkkkkk "<< endl;
	}
	// cout<< train_values[100][4]<< endl;
	pthread_mutex_destroy(&mutex_sum);
	pthread_exit(NULL);
}